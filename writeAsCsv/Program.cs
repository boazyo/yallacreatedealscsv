﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace writeAsCsv
{

    public class CsvRow : List<string>
    {
        public string LineText { get; set; }
    }

    /// <summary>
    /// Class to write data to a CSV file
    /// </summary>
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            StringBuilder builder = new StringBuilder();
            bool firstColumn = true;
            foreach (string value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(',');
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', ',' }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    builder.Append(value);
                firstColumn = false;
            }
            row.LineText = builder.ToString();
            WriteLine(row.LineText, Encoding.UTF8);
        }
    }


    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var sw = new StreamWriter(@"C:\Logs\excelService.txt", true, Encoding.UTF8))
            {
                string queryString = "SELECT TOP 1000 deals.Id, Name, NewPrice, DealDate, MainImage FROM[Fattal].[dbo].[Deals] as deals JOIN[Fattal].[dbo].[Hotels] as hotels on deals.HotelId = hotels.Id where Active = 1 and DealDate>= GETDATE() order by 1 desc";
                sw.Write("Start: ");
                // OleDbConnection db = new OleDbConnection("Provider = SQLOLEDB;Data Source=213.57.26.160;Initial Catalog=Fattal;User ID=FT-YALA2\\admin; Password=Qw123Boris");
                OleDbConnection connection = new OleDbConnection("Provider = SQLOLEDB;Data Source=127.0.0.1;Initial Catalog=Fattal;User ID=fattal-admin; Password=!groboot!fattal123");
                sw.Write("Set Connection. ");
                try
                {
                    OleDbCommand command = new OleDbCommand(queryString, connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();
                    File.Delete("WriteTest.csv");
                    using (CsvFileWriter writer = new CsvFileWriter("C:\\Users\\admin\\Desktop\\writeAsCsv\\writeAsCsv\\bin\\Release\\WriteTest.csv"))
                    {
                        CsvRow row3 = new CsvRow();
                        row3.Add(String.Format("{0}", "\ufeff מספר הדיל"));
                        row3.Add(String.Format("{0}", "שם המלון"));
                        row3.Add(String.Format("{0}", "מחיר"));
                        row3.Add(String.Format("{0}", "תאריך"));
                        row3.Add(String.Format("{0}", "תמונה"));
                        writer.WriteRow(row3);
                        while (reader.Read())
                        {
                            try
                            {
                                CsvRow row = new CsvRow();
                                row.Add(String.Format("{0}", reader.GetInt32(0)));
                                row.Add(String.Format("{0}", reader["Name"]));
                                row.Add(String.Format("{0}", reader["NewPrice"]));
                                row.Add(String.Format("{0}", reader["DealDate"]));
                                row.Add(String.Format("{0}", "https://s3.amazonaws.com/yallabucket/"+reader["MainImage"]));
                                writer.WriteRow(row);
                            }
                            catch (Exception e) {
                                sw.Write("write failed: " + e.Message);
                            }
                        }
                    }
                    // always call Close when done reading.
                    reader.Close();
                    sw.Write("Open Success");
                }
                catch (Exception e)
                {
                    sw.Write("Open failed: "+ e.Message);
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
